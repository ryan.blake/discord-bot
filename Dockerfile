    FROM node:latest
    WORKDIR /app

    # Install FFMPEG For node-opus
    RUN apt-get update \
        && apt-get install ffmpeg -y

    # Setup Work directory 
    COPY package.json config.json ./
    COPY media ./media

    # Install Node Modules
    RUN npm install
    
    # npm is dumb
    RUN cp -r ./node_modules/m3u8stream/dist ./node_modules/m3u8stream/lib

    # Copy project to our WORKDIR
    COPY src ./src
      
    # Let's run it!
    WORKDIR /app/src
    CMD [ "node", "bot.js" ]
