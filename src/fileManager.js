const request = require('request');
const { createWriteStream } = require('fs');
const logger = require('./logger');
const { readdir } = require('fs').promises;
const mediaDir = '../media/';

const uploadAudioFile = async (audioFile) => {
    const extension = audioFile.filename.split('.')[1];
    if(["wav", "mp3"].indexOf(extension) === -1)
        return;

    await request.get(audioFile.url)
        .on('error', error => logger.logError('Whoops! Couldn\'t upload %s, %s.\n', [audioFile.filename, error]))
        .on('end', () => logger.logInfo('[***%s has uploaded %s***]', [audioFile.message.author.username, audioFile.filename]))
        .pipe(createWriteStream(`${mediaDir}/${audioFile.filename}`));
}

const getAudioFiles = async () => await readdir(mediaDir);

module.exports = { uploadAudioFile, getAudioFiles };