const Discord = require('discord.js');
const bot = new Discord.Client();
const config = require('../config.json');
const logger = require('./logger');
const commandProcessor = require('./commandProcessor');
const fileManager = require('./fileManager');
const statusManager = require('./statusManager');
const memberManager = require('./memberManager');

bot.on('ready', async () => {
    logger.logInfo('[***%s has started***]', [statusManager.getBotName(bot)]);
    statusManager.setWaiting(bot);
    }).on('message', async message => {
        const msg = message.content;
        const attachment = message.attachments.first();

        if (attachment)
            await fileManager.uploadAudioFile(attachment);
        if (!msg.substring(0, 1).localeCompare(config.prefix))
            await commandProcessor.filterMemberCommand(message, bot).catch(error => logger.logError('Whoops! %s', [error]));
        if (config.filter.includes(msg.toLowerCase()))
            await memberManager.kickThisBisch(message);
    }).login(process.env.DISCORD_BOT_API_KEY)
        .then(x => logger.logInfo('[***%s has successfully logged in.***]', [bot.user.username]))
        .catch(error => logger.logError('Whoops! Had a problem logging in.:\n%s', [error]))
    .catch(error => logger.logError('Whoops! Seems like we weren\'t ready.:\n%s', [error]));