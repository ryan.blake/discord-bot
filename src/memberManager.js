const logger = require('./logger');

const kickThisBisch = async (msg) => {
    let reason = `saying some silly shit like this, "${msg.content}".`
    
    await msg.member
        .kick(reason)
        .then(async () => await msg.reply(`${msg.member.user.tag} has been kicked by ${msg.author.tag} for ${reason}`))
        .catch(async error => {
            await msg.reply(`I couldn't kick ${msg.member.user.tag}, there was a problem.`);
            logger.logError('Whoops! Couldn\'t kick %s, %s.\n', [msg.member.user.tag, error]);
        });
}

module.exports = { kickThisBisch };