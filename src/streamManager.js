const ytdl = require('ytdl-core');
const logger = require('./logger');
const statusManager = require('./statusManager');
const mediaDir = '../media/';

const controlStream = async (cmd, message, bot) => {
    const vc = bot.voiceConnections.find(vc => vc.channel.guild.id === message.guild.id);
    const dispatcher = vc.player.dispatcher;
    
    switch(cmd){
        case 'stop': {
            await dispatcher.end(); 
            return logger.logInfo('[***%s has left %s***]', [bot.user.username, vc.channel.name]);
        }     
        case 'pause': {
            await dispatcher.pause(); 
            return logger.logInfo('[***%s has paused stream***]', [bot.user.username]);
        }
        case "resume": {
            await dispatcher.resume(); 
            return logger.logInfo('[***%s has resumed streaming***]', [bot.user.username]);
        }        
    }
}

const playMediaFile = async (message, cmd, audioFiles, bot) =>{ 
    const audioFile = audioFiles.filter(f => f.split('.')[0] === cmd);
    await playAudio(message, cmd, audioFile, createAudioFileDispatcher, bot);
}

const playYouTubeStream = async (message, url, bot) => {
    if(!url || !ytdl.validateURL(url))
        return await message.reply(`missing or invalid YouTube URL.`);

    const streamTitle = await ytdl.getBasicInfo(url).then((info) => info.title);
    await playAudio(message, streamTitle, url, createYouTubeDispatcher, bot);
}

const playAudio = async (message, sourceTitle, source, createDispatcherCallback, bot) => {
    const vchannel = message.member.voiceChannel;

    if(!vchannel)
        return await notInVCMessage(message);

    await vchannel.join()
        .then(async connection => {
            const dispatcher = createDispatcherCallback(connection, source);

            await setStatusPlaying(sourceTitle, vchannel.name, bot);

            dispatcher.on('end', async () => {
                await endPlaying(vchannel, bot);
            });
        }).catch(async error => {
            await handleStreamException(error, bot);
    });
}

const setStatusPlaying = async (audioTitle, vchannelName, bot) =>{
    logger.logInfo('[***%s has started playing %s in %s***]', [statusManager.getBotName(bot), audioTitle, vchannelName]);
    await bot.user.setActivity(`${audioTitle} in ${vchannelName}.`, {type: "PLAYING"});
}

const endPlaying = async (vchannel, bot) => {
    vchannel.leave();
    logger.logInfo('[***%s has left %s***]', [statusManager.getBotName(bot), vchannel.name]);
    await statusManager.setWaiting(bot);
}

const handleStreamException = async (error, bot) => {
    logger.logError('Whoops! Couldn\'t play stream, %s.\n', [error]);
    await statusManager.setWaiting(bot);
}

const createYouTubeDispatcher = (connection, url) => connection.playStream(ytdl(url, { filter: 'audioonly' }));
const createAudioFileDispatcher = (connection, audioFile) => connection.playFile(`${mediaDir}/${audioFile}`);
const notInVCMessage = async (message) => await message.reply(`you gotta be in a voice channel, yo.`);

module.exports = { controlStream, playMediaFile, playYouTubeStream };