const config = require('../config.json');

const randomLenny = () => config.lenny[Math.floor(Math.random()*config.lenny.length)];
const setWaiting = async (bot) => await bot.user.setActivity(`you ${randomLenny()}.`, {type: "WATCHING"});
const getBotName = (bot) => { if (bot) {return bot.user.username} throw "Bot isn't ready"};

module.exports = { setWaiting, getBotName };