const streamManager = require('./streamManager');
const fileManager = require('./fileManager');
const config = require('../config.json');

let audioFilesList;

const filterMemberCommand = async (message, bot) => {
    const audioFiles = await fileManager.getAudioFiles();
    audioFilesList = audioFiles.map(f => f.split('.')[0]);
    const args = message.content.substring(1).split(' ');
    const cmd = args[0].toLowerCase();

    if (audioFilesList.indexOf(cmd) > -1) 
        return await streamManager.playMediaFile(message, cmd, audioFiles, bot);
    if (config.streamControls.indexOf(cmd) > -1) 
        return await streamManager.controlStream(cmd, message, bot);
        
    return await processMemberCommand(cmd, args, message, bot)
}

const processMemberCommand = async (cmd, args, message, bot) => {
    switch(cmd){
        case 'music':
            return await music(message);
        case 'yt':
            return await streamManager.playYouTubeStream(message, args[1], bot);
        default:
            return await help(message);
    }
}

const help = async (message) => await message.channel.send(config.helpMenu.join(''));
const music = async (message) => await message.channel.send([config.musicHelp, ...audioFilesList]);

module.exports = { filterMemberCommand };