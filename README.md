<h1>Prerequisites</h1>

* Node.js (if running via node.exe)
* Docker (if interested in running in Docker)
* ffmpeg (if running via node.exe)
* Python (if running via node.exe, needed for node-opus)

If you'll only be running this via Docker, the only dependecy you'll need to address is Docker itself.

<h1>Recommendations</h1>

* [Visual Studio Code](https://code.visualstudio.com/) or [Atom](https://atom.io/)
* [Chocolatey](https://chocolatey.org/) if working in Windows
    + [PowerShell]:&emsp;`Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`
* [Homebrew](https://brew.sh/) if working in MacOS
    + [Terminal]:&emsp;`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
* [Homebrew on Linux](https://docs.brew.sh/Homebrew-on-Linux) if working in Linux.
    + [bash]:&emsp;`sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"`
<h1>Instructions</h1>

**PowerShell - node.exe:**
```powershell
npm install

$env:DISCORD_BOT_API_KEY='<key-here>'; node src/bot.js
```
**bash - node.exe:**
```bash 
npm install

DISCORD_BOT_API_KEY='<key-here>' node src/bot.js
```
**Docker - any shell (omit '' around key if in cmd.exe):**
```bash
docker build -t <image-name> .

docker run -d --name <ContainerName> -e DISCORD_BOT_API_KEY='<key-here>' <image-name>
```
---

If you're using something like Octopus for your Docker deploys, you would set your env vars right in the step where you deploy your Docker image.  

I don't personally use Kubernetes, but I've included a link to a pretty decent looking resource for setting up environment variables:  
[Define Environment Variables for a Container - Kubernetes](https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/)  
Though, if you're using Kubernetes, I would assume you already know this.  

More of the same for Azure Container Instances:  
[Set Environment Variables in Container Instances | Microsoft Docs](https://docs.microsoft.com/en-us/azure/container-instances/container-instances-environment-variables)

---

You may put the API key directly in the .env file, **I don't recommend this** as it's not very secure.

Big thanks to Benjamin Hoffmeyer for this amazing resource, [Creating a discord bot & getting a token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token).